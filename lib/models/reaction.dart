import 'package:facebook/models/user.dart';

class Reaction {
  User user;
  ReactionType reactionType;

  Reaction({this.user, this.reactionType});
}

enum ReactionType { LIKE, ANGRY, LOVE, CARE, HAHA, WOW, SAD }

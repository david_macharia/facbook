import 'message_content.dart';

class Message{
  final MessageContent content;
  final bool isRead;
  final isMine;
  const Message({this.content,this.isRead=false,this.isMine=false});
}
import 'package:facebook/tools/SizeConfig.dart';
import 'package:flutter/material.dart';

class Notification {
   Icon notificationIcon;
   final String notifactionLargeIcon;
  final String notificationSource;
  final NotificationType notificationType;
  final NotificationState notificationState;
  final String notifacationTime;
  final bool isRead;
   Notification(
      { this.notifactionLargeIcon,
        this.isRead=true,
        this.notifacationTime="Now",
        this.notificationState=NotificationState.OLD, this.notificationSource, @required this.notificationType}){
    switch(notificationType){

      case NotificationType.LIVE_VIDEO:
        double width=SizeConfig.screenWidth;
        notificationIcon=Icon(Icons.play_circle_filled,size: width*0.07,   color: Colors.deepPurple,);
        // TODO: Handle this case.
        break;
      case NotificationType.JOB:
        notificationIcon=Icon(Icons.work,   color: Colors.deepPurple,);

        break;
      case NotificationType.STORY_LINE_IMAGE:
        // TODO: Handle this case.
        break;
      case NotificationType.GROUP:
        // TODO: Handle this case.
        break;
      case NotificationType.COMMENT:
        // TODO: Handle this case.
        break;
      case NotificationType.BIRTH_DAY:
        // TODO: Handle this case.
        break;
      case NotificationType.NEW_PHOTO:
        // TODO: Handle this case.
        break;
      case NotificationType.REACTION:
        // TODO: Handle this case.
        break;
    }
  }
}
enum NotificationState{
   NEW,
  OLD
}
enum NotificationType {
  LIVE_VIDEO,
  JOB,
  STORY_LINE_IMAGE,
  GROUP,
  COMMENT,
  BIRTH_DAY,
  NEW_PHOTO,
  REACTION,
}

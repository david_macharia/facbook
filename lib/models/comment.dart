import 'package:facebook/models/reaction.dart';
import 'package:facebook/models/user.dart';

class Comment {
  String comment;
  List<User> tags;
  List<Reaction> reactions;
  double id;
  Comment parentComment;
}

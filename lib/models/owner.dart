import 'package:facebook/models/user.dart';

class Owner extends User{
  Owner({String firstName, String lastName, profileImage, pages}):super(firstName:firstName,lastName:lastName,profileImage:profileImage,pages:pages);

}
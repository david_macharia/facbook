import 'dart:math';

import 'package:facebook/models/comment.dart';
import 'package:facebook/models/owner.dart';
import 'package:facebook/models/reaction.dart';
import 'package:flutter/cupertino.dart';
import 'package:video_player/video_player.dart';

class Video {
  int id;
  String videoUrl;
  String videoDescription;
  List<Reaction> reactions;
  List<Comment> comments;
 VideoState videoState;
  VideoPlayerController controller;
  Owner owner;
  Video({@required this.videoUrl, this.videoDescription, this.reactions, this.comments,this.owner})  {
    id=Random.secure().nextInt(10000);
    controller=VideoPlayerController.asset(this.videoUrl);

  }
  isPlaying(){
    if(videoState==VideoState.PLAY){
      return true;
    }
    return false;
  }
  play(){
    if(!isPlaying()){
      videoState=VideoState.PLAY;
    }
  }

  void stop() {
    if(isPlaying()){
      videoState=VideoState.STOP;
    }
  }
}
enum VideoState{
  PLAY,
  PAUSE,
  STOP
}
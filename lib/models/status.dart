class Status {
  final String statusSource;
  final bool viewed;
  const Status({this.statusSource,this.viewed});
}

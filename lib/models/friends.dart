import 'package:facebook/models/message.dart';
import 'package:facebook/models/status.dart';

class Friend {
  final String friendName;
  final String profileImage;
  final bool isFrequentContacted;
  final bool isAFriend;
  final bool friendRequestSent;
  final bool friendRequestReceived;
  final bool isActive;
  final String lastSeen;
  final List<Friend> matualFriends;
  final List<Status> status;
  final List<Message> messages;
  const Friend(
      {this.friendName = "",
      this.profileImage =
          "https://scontent.fnbo4-1.fna.fbcdn.net/v/t1.30497-1/cp0/c24.0.80.80a/p80x80/84688533_170842440872810_7559275468982059008_n.jpg?_nc_cat=1&_nc_sid=dbb9e7&_nc_eui2=AeH18kU0Y7r6jGLnZfI4GSrSwBqKCN3Pzv_AGooI3c_O_2nHHfLQ-1K1QnBxxnOUbYLiw-UP8k6o6LLxmcUIanZp&_nc_oc=AQkXSZSpYn374N4ZQE63SrRu0lHTtaHQd4GXeygmvy1aiKArj2nkPU4Wd7NgLuCTRG0&_nc_ht=scontent.fnbo4-1.fna&oh=3439366609b6b2afa4dc020929a00a33&oe=5F1FE551",
      this.isFrequentContacted = false,
      this.friendRequestReceived = false,
      this.friendRequestSent = false,
      this.isAFriend = false,
      this.isActive = false,
      this.lastSeen,
      this.status = const [],
      this.messages = const [],
      this.matualFriends = const []});
  bool isStatusViewed() {
    return status.where((status) {
          return status.viewed == false;
        }).length >
        0;
  }
  Message getFirstMessage(){
    return this.messages.first;
  }
}

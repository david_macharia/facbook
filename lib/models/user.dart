import 'package:facebook/models/user_page.dart';

class User{
  String _firstName;
  String _lastName;
  String profileImage;
  List<Page> pages;

  User({String firstName, String lastName, this.profileImage, this.pages=const []}){
    this._firstName=firstName;
    this._lastName=lastName;
  }
  getFullName(){
    return "${this._firstName +" "+this._lastName}";
  }

}
import 'package:facebook/models/message_content.dart';

class TextMessage extends MessageContent{
  final String message;
  const TextMessage({this.message}):super(messageType:MessageType.PLAIN_TEXT);


}
import 'package:facebook/models/friends.dart';
import 'package:facebook/models/message.dart';
import 'package:facebook/models/notification.dart';
import 'package:facebook/models/status.dart';
import 'package:facebook/models/text_message.dart';

List<Notification> _notifications = [
  Notification(
    notifacationTime: "5 hours Ago",
      notificationType: NotificationType.LIVE_VIDEO,
   notifactionLargeIcon: "https://scontent.fnbo10-1.fna.fbcdn.net/v/t1.0-1/cp0/p48x48/17903454_10158949965035725_3181251005684687258_n.jpg?_nc_cat=1&_nc_sid=dbb9e7&_nc_eui2=AeG8kv11JHePwKvoqktD94EGRhbPH0xNK7VGFs8fTE0rtZCHgrnMZOPN3JZHnQiehVWG1krTVAwcMLSYdmYT56GB&_nc_oc=AQkyLbJqgW8ZOfLKgcIyXyDrk90UTwpK_4OR7UZ9ToFLvg9aCk6eriKYLMr9FW1h3eHB8jyTq89d5yh3s05oLVDw&_nc_pt=5&_nc_ht=scontent.fnbo10-1.fna&oh=037d55ac3859bd3a4e0f193af81846c3&oe=5F2EAC13",
      notificationSource:
          "WATCH: Spotlight: New Hampshire with Corey Lewandowski, Pam Tucker, and Ray Chadwick!"),

  Notification(
      notifacationTime: "5 hours Ago",
      notificationType: NotificationType.LIVE_VIDEO,

      notificationSource:
      "CGTN is live now:Live:Wild crested ibises and their lovely newborn "),
  Notification(
      notificationType: NotificationType.JOB,
      isRead: false,
      notificationState: NotificationState.NEW,
    notifactionLargeIcon: "https://scontent.fnbo9-1.fna.fbcdn.net/v/t1.0-0/cp0/c43.0.50.50a/p50x50/20264710_1569000826485804_585585535504035224_n.jpg?_nc_cat=110&_nc_sid=ca434c&_nc_eui2=AeGojgziZpPXXnYRnAcYSFyG66dNglNRR5rrp02CU1FHmr4d3CxLHm9J05IpXNv7Na4mEA45X5KelO63nyuTnd-8&_nc_oc=AQn6OEWDgLlu-cJ5xmQ3IR5LlHwKBXCjD9rifdWYC2EBk2L5qb-NbxlcXnXuOuV2LDsGlXLKQVG46FI5A5d3eYM2&_nc_pt=5&_nc_ht=scontent.fnbo9-1.fna&oh=15fd8589688c3810ff046b6c74147345&oe=5F321706",
      notificationSource:
          "Why should I use Flutter instead of React Native?"),
  Notification(
      notifacationTime: "5 hours Ago",
      notificationType: NotificationType.LIVE_VIDEO,
      notificationSource:
      "CGTN is live now:Live:Wild crested ibises and their lovely newborn "),
  Notification(
      notifacationTime: "5 hours Ago",
      notificationType: NotificationType.LIVE_VIDEO,
      notificationSource:

      "CGTN is live now:Live:Wild crested ibises and their lovely newborn "),
  Notification(
      notifacationTime: "5 hours Ago",
      notificationType: NotificationType.LIVE_VIDEO,
      notificationSource:
      "CGTN is live now:Live:Wild crested ibises and their lovely newborn "),
  Notification(
      notifacationTime: "1d",
      notificationType: NotificationType.JOB,
      notifactionLargeIcon: "https://scontent.fnbo9-1.fna.fbcdn.net/v/t1.0-1/p200x200/107664225_711133186285687_7143207052938772239_n.jpg?_nc_cat=105&_nc_sid=dbb9e7&_nc_eui2=AeFllz_kpMZ47fb98mMVMnKQPx-pPwoU8ks_H6k_ChTySyXIwqkmvjNlJ1Tjmx4M6EGqqVA0Rv61rfd8qIIg52_Z&_nc_oc=AQmayNTByFdh9Cj7szlAwFMemR27rPQqwAu-ZNQxDO1-ZFbCB8ZDs6z-tGfMeWiKU0a3PcEX3xUgegBDjP3YFzod&_nc_pt=5&_nc_ht=scontent.fnbo9-1.fna&_nc_tp=6&oh=bc20331171a5d66054e45d1e866e0f23&oe=5F32204C",
      notificationSource:
      "2022 Nyeri Governor Campaign Financer Logistics"),
  Notification(
      notificationType: NotificationType.JOB,
      notificationState: NotificationState.OLD,

      notificationSource:
      "CGTN is live now:Live:Wild crested ibises and their lovely newborn ")
];
getNotification(){
  _notifications.sort((a,b){
    if(a.notificationState ==b.notificationState){
      return 2;
    }else if(a.notificationState ==NotificationState.NEW){
      return -1;
    }else{
      return 1;
    }
  });
  return[..._notifications];
}

List<Notification>getNewNotifications(){
  return [..._notifications.where((notification){
    return notification.notificationState==NotificationState.NEW;
  }).toList()];
}
final friends = const [
  const Friend(
      friendName: "DJMalik- Arh",
      friendRequestReceived: true,
      isActive: true,
      messages: [
        const Message(
            isRead: false,
            content: TextMessage(message: "Nilikutumia Video whats app"))
      ],
      status: [
        Status(
            statusSource:
                "https://scontent.fnbo4-1.fna.fbcdn.net/v/t1.0-9/107669580_124374232652854_1913119213111753985_o.jpg?_nc_cat=107&_nc_sid=5b7eaf&_nc_eui2=AeF0wTtBB_7b2og19ANE-W-F05DcdJJh7urTkNx0kmHu6rtsHieG0doJN4l5hbvjuSouXie3t7zL4ENkgv9ClV1M&_nc_oc=AQm5d-k3GV5h6JOCJ9z5jSrzvZ820x160SxrtP3qP2aYHKbsndZLEFjPmMTXTCY5I6o&_nc_ht=scontent.fnbo4-1.fna&oh=b6bc7678c4f63974704b222247192414&oe=5F31E1B5",
            viewed: false),
      ],
    profileImage: "https://scontent.fnbo4-1.fna.fbcdn.net/v/t1.0-9/107669580_124374232652854_1913119213111753985_o.jpg?_nc_cat=107&_nc_sid=5b7eaf&_nc_eui2=AeF0wTtBB_7b2og19ANE-W-F05DcdJJh7urTkNx0kmHu6rtsHieG0doJN4l5hbvjuSouXie3t7zL4ENkgv9ClV1M&_nc_oc=AQm5d-k3GV5h6JOCJ9z5jSrzvZ820x160SxrtP3qP2aYHKbsndZLEFjPmMTXTCY5I6o&_nc_ht=scontent.fnbo4-1.fna&oh=b6bc7678c4f63974704b222247192414&oe=5F31E1B5"
),
  const Friend(
      friendName: "Dhar Mann",
      friendRequestReceived: true,
      messages: [
        const Message(
            isRead: false,
            content: TextMessage(message: "Nilikutumia Video whats app"))
      ],
      status: [
        Status(
          statusSource: "https://scontent.fnbo4-1.fna.fbcdn.net/v/t1.0-9/s1080x2048/107834510_2684111535236374_4320016651305042388_o.jpg?_nc_cat=1&_nc_sid=5b7eaf&_nc_eui2=AeH0HSXeQYln4MSPlrN6D5ZlPV7vgGE9GAY9Xu-AYT0YBnMYeqR1HcSXdXBAXoyzn_rldnHr2uQQxQGtXacUBjYm&_nc_oc=AQkk0HwDDenUeQZrtiQXHTVYpsb3TnmBARGtwGDLY72ROGLd_ny-Pamz3ouPRQNrO10&_nc_ht=scontent.fnbo4-1.fna&_nc_tp=7&oh=716f9b4d3e3ad23648740bc27efbd639&oe=5F3108E3",
          viewed: false),
      ],
    profileImage: "https://scontent.fnbo4-1.fna.fbcdn.net/v/t1.0-1/cp0/p40x40/48379286_2129454880702045_3666923913457172480_o.jpg?_nc_cat=1&_nc_sid=1eb0c7&_nc_eui2=AeHDG8W5jEdbOvUGdTzALeclJBjktlXTfRUkGOS2VdN9FTzOq9Y8kTz3fiR7woWpF6RCjvFNI7qxci2EA8K8C5uC&_nc_oc=AQl8cuUErpn-cmhHh4EpG2oMCxMXnYSHlAuYRGpjQP49DUPva4h-Cn0ZS5Ig_-lceY4&_nc_ht=scontent.fnbo4-1.fna&oh=1925824bcad0232132f0d532bf351fcb&oe=5F328B37"
  ),
  const Friend(
      friendName: "Alicialopez Wambugu",
      friendRequestReceived: true,
      lastSeen: "18m",
      messages: [
        const Message(
            isRead: false,
            content: TextMessage(message: "Nilikutumia Video whats app"))
      ],
      status: [
        Status(
          statusSource: "https://scontent.fnbo4-1.fna.fbcdn.net/v/t1.0-9/108222645_362242384772077_5264764630653024829_o.jpg?_nc_cat=109&_nc_sid=5b7eaf&_nc_eui2=AeHyNfBTN2759mil3x4X2kHXk-Frw7OaiBaT4WvDs5qIFiHNLFirx_eQX6k_VDPbGwVPMstmHFwK_Kju56g3M_Zo&_nc_oc=AQny1i4vwKfsicyCBBmiYyP4MDF9U9jAdh6ezBfrRWtADGTm0FhvbT7i-vvHT1ZzFIo&_nc_ht=scontent.fnbo4-1.fna&oh=7d060d6a326af08fc367c0196a32f343&oe=5F302655",
            viewed: false),
      ],
    profileImage: "https://scontent.fnbo4-1.fna.fbcdn.net/v/t1.0-1/c0.0.160.160a/p160x160/97092405_321887322140917_8716107386693091328_o.jpg?_nc_cat=105&_nc_sid=dbb9e7&_nc_eui2=AeHtDy1luEZceqe-G1R4zY8o-7B63vAPwNT7sHre8A_A1LHd9jrQNW65PLF6GVPMfLYH2ZQk3ZExnkdz2ss4fG82&_nc_oc=AQnEhf6szRY90aHeXaoN_Nwuuve63L0pASPyY6qd3LfiyFkIQx_hcFth0M5Jdnwf7wQ&_nc_ht=scontent.fnbo4-1.fna&oh=bd37a976564f537ceedfa4cbfa314a26&oe=5F32E4CB"
  ),
  const Friend(
      friendName: "Precious Candis",
      friendRequestReceived: true,
      messages: [
        const Message(
            isRead: false,
            content: TextMessage(message: "Nilikutumia Video whats app"))
      ],
      status: [
        Status(
          statusSource: "https://scontent.fnbo4-1.fna.fbcdn.net/v/t1.0-9/s1080x2048/109075742_325503958466561_7231666164563977681_o.jpg?_nc_cat=110&_nc_sid=5b7eaf&_nc_eui2=AeGxgRT0oES6Ri9tOW6iVLrgSF150mRRXFtIXXnSZFFcW4B2-NZpLNC72gCQ6ZWqmtcF0gX1maWT9BkaqWENT-dG&_nc_oc=AQk77OUlGLQWStYC7tuvQ2A3dccLZ00H6xpYJticcRoeiHJ6ML_2IddoHgSxsbEq5ZU&_nc_ht=scontent.fnbo4-1.fna&_nc_tp=7&oh=2ef54605052093423b31fd3b78cac813&oe=5F32683E",
            viewed: false),
      ],
profileImage: "https://scontent.fnbo4-1.fna.fbcdn.net/v/t1.0-9/109123428_325497338467223_5586352427814330512_n.jpg?_nc_cat=110&_nc_sid=110474&_nc_eui2=AeFcoPWxFojG8ESAU4G8DBFMMKdwu4iHx5Uwp3C7iIfHlTvNimFtplly3vFTXthTfPtolzu-HY3Vo9lrS-OAXnws&_nc_oc=AQlj_0Bc7xGOmxE69sjhVUjdX4pXGCepmktdobCLMKXI6jw2pVoa-z62zcbfqxDXR3M&_nc_ht=scontent.fnbo4-1.fna&oh=e2a4d6dd6c6f6db825fe788612e74a6b&oe=5F2F68DA"
  ),
 //const Friend(friendName: "Manal Queen",profileImage: "https://scontent.fnbo4-1.fna.fbcdn.net/v/t1.0-1/cp0/p80x80/106168766_150894369901135_5319189600738742799_n.jpg?_nc_cat=103&_nc_sid=dbb9e7&_nc_eui2=AeEDK1GGvjVJqSHxG0czYL5cNqVibjncs_c2pWJuOdyz9zh6_9ITOIiutgroaraQ1GnTwT6jYsfrhhmAU65KxAvz&_nc_oc=AQmQKrtjZdp9_1t9_heEPkEMmB_HDnQdVjjVSJq4zdanlYQ4N0EAHDoaja7i9ix2zTA&_nc_ht=scontent.fnbo4-1.fna&oh=3a23e667eb4b2cf675ba0d49a10b5aed&oe=5F21B63D"),
  // const Friend(friendName: "Mary Njihia"),
];
hasFriendRequest() {
  var count = friends.where((f) => f.friendRequestReceived).length;
  return count == 0 ? -1 : count;
}

List<Friend> getFriendsWithStatus() {
  return friends.where((friend) {
    return friend.status.length > 0;
  }).toList();
}

List<Friend> getFriendWithMessages() {
  return friends.where((friend) {
    return friend.messages.length > 0 ? true : false;
  }).toList();
}

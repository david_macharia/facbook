import 'package:facebook/contants/page.dart';
import 'package:facebook/tools/SizeConfig.dart';
import 'package:flutter/material.dart';

typedef OnPageSelected({@required NavigationPage page});

class PageNavigation extends StatefulWidget {
  final OnPageSelected onPageSelected;

  PageNavigation({Key key, this.onPageSelected}) : super(key: key);

  @override
  _PageNavigationState createState() => _PageNavigationState();
}

class _PageNavigationState extends State<PageNavigation> {
  NavigationPage active = NavigationPage.HOME;

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    double height = SizeConfig.screenHeight;
    double width = SizeConfig.screenWidth;

    return Container(
      height: height * 0.08,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Container(
            width: width * 0.1,
            child: Stack(
              children: <Widget>[
                GestureDetector(
                  onTap: (){
                    widget.onPageSelected(page:NavigationPage.HOME);
                    setState(() {
                       active=NavigationPage.HOME;
                    });
                   
                  },
                  child: Icon(
                    Icons.home,
                    size: height * 0.055,
                    color: active == NavigationPage.HOME ? Colors.blue : Colors.grey,
                  ),
                ),
                Positioned(
                  left: width * 0.04,
                  top: 0,
                  child: Container(
                    height: height * 0.029,
                    width: height * 0.029,
                    alignment: Alignment.center,
                    child: Text(
                      "1",
                      style: TextStyle(
                          color: Colors.white, fontSize: width * 0.035),
                    ),
                    decoration: BoxDecoration(
                        color: Colors.red,
                        border: Border.all(color: Colors.white),
                        borderRadius: BorderRadius.circular(50)),
                  ),
                )
              ],
            ),
          ),
          GestureDetector(
             onTap: (){
                    widget.onPageSelected(page:NavigationPage.FRIENDS);
                    setState(() {
                       active=NavigationPage.FRIENDS;
                    });
                   
                  },
                      child: Icon(
              active != NavigationPage.FRIENDS?Icons.people_outline:Icons.people,
              size: height * 0.05,
              color: active == NavigationPage.FRIENDS ? Colors.blue : Colors.grey,
            ),
          ),
          GestureDetector(
             onTap: (){
                    widget.onPageSelected(page:NavigationPage.MESSAGES);
                    setState(() {
                       active=NavigationPage.MESSAGES;
                    });
                   
                  },
                      child: Image.asset(
              "assets/images/messanger_icon.png",
              height: width * 0.19,
              width: width * 0.19,
              color: active == NavigationPage.MESSAGES ? Colors.blue : Colors.grey,
            ),
          ),
          GestureDetector(
             onTap: (){
                    widget.onPageSelected(page:NavigationPage.VIDEOS);
                    setState(() {
                       active=NavigationPage.VIDEOS;
                    });
                   
                  },
                      child: Icon(
              Icons.ondemand_video,
              size: height * 0.05,
              color: active == NavigationPage.VIDEOS ? Colors.blue : Colors.grey,
            ),
          ),
          GestureDetector(
             onTap: (){
                    widget.onPageSelected(page:NavigationPage.NOTIFICATIONS);
                    setState(() {
                       active=NavigationPage.NOTIFICATIONS;
                    });
                   
                  },
                      child: Transform.rotate(
              angle: -0.2,
              child: Icon(
                Icons.notifications_none,
                size: height * 0.06,
                color: active == NavigationPage.NOTIFICATIONS ? Colors.blue : Colors.grey,
              ),
            ),
          ),
          GestureDetector(
             onTap: (){
                    widget.onPageSelected(page:NavigationPage.SETTING);
                    setState(() {
                       active=NavigationPage.SETTING;
                    });
                   
                  },
                      child: Icon(
              Icons.menu,
              size: height * 0.06,
              color: active == NavigationPage.SETTING ? Colors.blue : Colors.grey,
            ),
          ),
        ],
      ),
    );
  }
}

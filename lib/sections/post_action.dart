
import 'package:facebook/tools/SizeConfig.dart';
import 'package:flutter/material.dart';

class PostAction extends StatelessWidget {
  const PostAction({
    Key key,
  
  }) : super(key: key);

  

  @override
  Widget build(BuildContext context) {
      double height = SizeConfig.screenHeight;
    double width = SizeConfig.screenWidth;

    return Container(
     
      height: height * 0.1,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
              child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            FlatButton.icon(
                onPressed: null,
                icon: Image.asset(
                  "assets/images/write_book.png",
                  width: width*0.07,
                  height: width*0.07,
                  color: Colors.blue,
                ),
                label: Text(
                  "Text",
                  style: TextStyle(fontSize: width*0.06),
                )),
            FlatButton.icon(
                onPressed: null,
                icon: Icon(
                  Icons.video_call,
                  color: Colors.red,
                  size: width * 0.09,
                ),
                label: Text(
                  "Video",
                  style: TextStyle(fontSize: width*0.06),
                )),
            FlatButton.icon(
                onPressed: null,
                icon: Icon(
                  Icons.location_on,
                  color: Colors.red,
                  size: width * 0.09,
                ),
                label: Text(
                  "Location",
                  style: TextStyle(fontSize: width*0.06),
                ))
          ],
        ),
      ),
    );
  }
}

import 'package:facebook/tools/SizeConfig.dart';
import 'package:flutter/material.dart';

class CreatePost extends StatelessWidget {
  const CreatePost({
    Key key,

  }) : super(key: key);

 

  @override
  Widget build(BuildContext context) {
        double width = SizeConfig.screenWidth;

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          Container(
              child: Row(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    width: width * 0.14,
                    height: width * 0.14,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(
                          50,
                        ),
                        border: Border.all(color: Colors.black)),
                    child: CircleAvatar(
                      minRadius: width * 0.07,
                      backgroundImage: NetworkImage(
                          "https://scontent.fnbo4-1.fna.fbcdn.net/v/t1.0-9/92516457_1516712285162897_7055747972943839232_n.jpg?_nc_cat=104&_nc_sid=85a577&_nc_eui2=AeHNBup4AlvkP3o5BNm14h_ZinKdDvbjnn6Kcp0O9uOefowujMifES-ipE6m9vudqdz8f5RJ6mQ22aVhWbLE5kV-&_nc_oc=AQmbL1GTVkQjcpdpR1Nvj6qhEIow2s9nTvO5xk1JCA_LxfLD2Ki0JBoFSCYji8pVMyo&_nc_ht=scontent.fnbo4-1.fna&oh=8f83cecf89c4392bd405a964df8b7bbe&oe=5F1E9862"),
                    ),
                  ),
                  Positioned(
                    bottom: 0,
                    right: 0,
                    child: Container(
                      width: width * 0.04,
                      height: width * 0.04,
                      decoration: BoxDecoration(
                          color: Colors.green,
                          borderRadius: BorderRadius.circular(
                            50,
                          ),
                          border: Border.all(
                              color: Colors.white, width: 2)),
                    ),
                  )
                ],
              ),
              Flexible(
                  child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  minLines: 2,
                  maxLines: 3,
                  style:TextStyle(fontSize: width*0.03) ,
                  decoration: InputDecoration(
                      hintText: "What's on your mind?",
                      
                      hintStyle: TextStyle(fontSize: width*0.06),
                      border: InputBorder.none),
                ),
              )),
              Container(
                height: width * 0.14,
                color: Colors.black54,
                width: 1,
              ),
              Container(
                width: width * 0.15,
                child: Column(
                  children: <Widget>[
                    Icon(
                      Icons.image,
                      size: width * 0.1,
                    ),
                    Text(
                      "Photo",
                      style: TextStyle(
                          fontSize: width*0.04, color: Colors.black54),
                    )
                  ],
                ),
              )
            ],
          )),
          Divider()
        ],
      ),
    );
  }
}

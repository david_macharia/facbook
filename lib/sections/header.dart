
import 'package:facebook/tools/SizeConfig.dart';
import 'package:flutter/material.dart';

class Header extends StatelessWidget {
  const Header({
    Key key,
 
  }) : super(key: key);


  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    double height = SizeConfig.screenHeight;
    double width = SizeConfig.screenWidth;

    return Container(
      height: height * 0.10,
     
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          CircleAvatar(
            maxRadius: height * 0.03,
            backgroundColor: Colors.black,
            child: Icon(
              Icons.account_circle,
              color: Colors.white,
              size: height * 0.06,
            ),
          ),
          Container(
            width: width * 0.65,
            margin: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.search,
                  size: height * 0.03,
                ),
                Container(
                  width: width * 0.35,
                  child: TextFormField(
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                        fillColor: Colors.white,
                        border: InputBorder.none,
                        hintText: "Search Facebook",
                        hintStyle: TextStyle(fontSize: width*0.03),
                        focusedBorder: InputBorder.none),
                  ),
                ),
              ],
            ),
            decoration: BoxDecoration(
                color: Colors.grey[300],
                borderRadius: BorderRadius.circular(50)),
          ),
          Container(
              child: Icon(
            Icons.photo_camera,
            size: width * 0.09,
          ))
        ],
      ),
    );
  }
}

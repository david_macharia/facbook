import 'package:facebook/contants/page.dart';
import 'package:facebook/models/setting_actions.dart';
import 'package:facebook/models/user.dart';
import 'package:facebook/models/user_page.dart';
import 'package:facebook/tools/SizeConfig.dart';
import 'package:flutter/material.dart';

class SettingSection extends StatefulWidget {
  @override
  _SettingSectionState createState() => _SettingSectionState();
}

class _SettingSectionState extends State<SettingSection> {
  User user;
  List<ActionCenter> actionCenters;
  @override
  void initState() {
    user = User(firstName: "Abdul", lastName: "Raheem",
        profileImage: "https://scontent.fnbo9-1.fna.fbcdn.net/v/t1.0-1/c0.0.160.160a/p160x160/92516457_1516712285162897_7055747972943839232_n.jpg?_nc_cat=104&_nc_sid=dbb9e7&_nc_eui2=AeHNBup4AlvkP3o5BNm14h_ZinKdDvbjnn6Kcp0O9uOefowujMifES-ipE6m9vudqdz8f5RJ6mQ22aVhWbLE5kV-&_nc_oc=AQmZX49NSJEPJ3CnOsWgxMwpgG88YUEiXJuc21o9BIu-ZbT0v-8JfW7i-nPi_BH88x4PrNNe-3ERV4q2h6ZMy6hu&_nc_pt=5&_nc_ht=scontent.fnbo9-1.fna&oh=64bcdf7addadf6e39b4903d8c81fff21&oe=5F3133A4",
        pages: [
      Page(title: "Tech We  Consume", isMine: true,iconUrl: "https://scontent.fnbo10-1.fna.fbcdn.net/v/t1.0-1/p200x200/90715365_675265879914408_6339746305878786048_n.png?_nc_cat=101&_nc_sid=dbb9e7&_nc_eui2=AeEDUFYFve-xhmnF7gic1LzaPS3kpea9cMM9LeSl5r1ww8gj2Vv5Exr8OTaGFcjlXe6nDtxrSmG3iUKRLDuuJ_8W&_nc_oc=AQkOkCksn0nqgngYVWj79GmlfCN0LY8JEXdMV7hB8i5qZR8296y9cWNANH-iqE24KnWrwjNMNi7uXXt2-tgo0MpC&_nc_pt=5&_nc_ht=scontent.fnbo10-1.fna&oh=f4c54d7f837462e3dfc01575d6de001a&oe=5F2ED66D"),
    ]);
    actionCenters = [
      ActionCenter(actioName: "Covid-19 Information Center",actionIcon: "https://web.facebook.com/images/comet/Circle-Covid19-Health-Heros-Hashtag.png"),
      ActionCenter(actioName: "Live Videos",actionIcon: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQWz9GvEk7FliTd5v1L_9dwGZfmZ-cFQmDxJQ&usqp=CAU"),
      ActionCenter(actioName: "Messages",actionIcon: "https://cdn2.iconfinder.com/data/icons/social-media-2285/512/1_Messenger_colored_svg-512.png"),
      ActionCenter(actioName: "Groups",actionIcon: "https://banner2.cleanpng.com/20180717/cek/kisspng-computer-icons-desktop-wallpaper-team-concept-5b4e0cd3819810.4507019915318417475308.jpg"),
      ActionCenter(actioName: "Friends",actionIcon: "https://cdn3.iconfinder.com/data/icons/facebook-ui-flat/48/Facebook_UI-26-512.png"),
      ActionCenter(actioName: "Videos"),
      ActionCenter(actioName: "Pages"),
      ActionCenter(actioName: "Saved"),

    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    double width = SizeConfig.screenWidth;
    double height = SizeConfig.screenHeight;
    return Container(
        color: Colors.white,
        child: ListView.separated(
            padding: EdgeInsets.all(0),
            itemBuilder: (ctx, count) {
              if (count == 0) {
                return Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: height * 0.00, horizontal: width * 0.03),
                  child: Row(
                    children: <Widget>[
                      CircleAvatar(
                        minRadius: width * 0.07,
                        maxRadius: width * 0.07,
                        backgroundImage: user.profileImage!=null?NetworkImage(user.profileImage):null,
                      ),
                      Container(
                        height: height * 0.09,
                        margin: EdgeInsets.symmetric(
                            vertical: height * 0.01, horizontal: width * 0.03),
                        child: Flex(
                          direction: Axis.vertical,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Expanded(
                                child: Text(
                              user.getFullName(),
                              style: TextStyle(fontSize: width * 0.05),
                            )),
                            Text("View Your profile",
                                style: TextStyle(fontSize: width * 0.05))
                          ],
                        ),
                      )
                    ],
                  ),
                );
              } else if (count > 0 && count <= user.pages.length) {
                Page page = user.pages[count - 1];
                return PageWidget(height: height, width: width, page: page);
              } else {
                ActionCenter actionCenter = actionCenters[count -2];
                return ActionCenterWidget(
                  actionCenter: actionCenter,
                );
              }
            },
            separatorBuilder: (ctx, count) {
              if (count == user.pages.length) {
                return Divider(
                  indent: width * 0.02,
                  endIndent: width * 0.02,
                );
              }
              return SizedBox();
            },
            itemCount: actionCenters.length+ user.pages.length + 1));
  }
}

class ActionCenterWidget extends StatelessWidget {
  final ActionCenter actionCenter;
  const ActionCenterWidget({
    this.actionCenter,
  });

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    double width = SizeConfig.screenWidth;
    double height = SizeConfig.screenHeight;
    return Padding(
      padding: EdgeInsets.symmetric(
          vertical: height * 0.00, horizontal: width * 0.03),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          CircleAvatar(
            minRadius: width * 0.07,
            maxRadius: width * 0.07,
            backgroundImage:actionCenter.actionIcon!=null? NetworkImage(actionCenter.actionIcon):null
          ),
          Container(
            height: height * 0.09,
            alignment: Alignment.center,
            margin: EdgeInsets.symmetric(
                vertical: height * 0.02, horizontal: width * 0.03),
            child: Text(
              actionCenter.actioName,
              style: TextStyle(fontSize: width * 0.05),
            ),
          ),
          Spacer(),
        ],
      ),
    );
  }
}

class PageWidget extends StatelessWidget {
  const PageWidget({
    Key key,
    @required this.height,
    @required this.width,
    @required this.page,
  }) : super(key: key);

  final double height;
  final double width;
  final Page page;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          vertical: height * 0.00, horizontal: width * 0.03),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          CircleAvatar(
            minRadius: width * 0.07,
            maxRadius: width * 0.07,
            backgroundImage: page.iconUrl!=null?NetworkImage(page.iconUrl):null,
          ),
          Container(
            height: height * 0.09,
            alignment: Alignment.center,
            margin: EdgeInsets.symmetric(
                vertical: height * 0.02, horizontal: width * 0.03),
            child: Text(
              page.title,
              style: TextStyle(fontSize: width * 0.05),
            ),
          )
        ],
      ),
    );
  }
}

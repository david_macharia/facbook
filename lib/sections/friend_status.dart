import 'package:facebook/contants/dummysources.dart';
import 'package:facebook/models/friends.dart';
import 'package:facebook/models/user.dart';
import 'package:facebook/tools/SizeConfig.dart';
import 'package:flutter/material.dart';

class FriendStatus extends StatelessWidget {

   FriendStatus({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = SizeConfig.screenHeight;
    double width = SizeConfig.screenWidth;

    return Container(
     
      height: height * 0.25,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: friends.length,
          itemBuilder: (buidContext, count) {
            Friend friend=friends[count];
            return StatusCard(index: count,friend:friend);
          }),
    );
  }
}

class StatusCard extends StatelessWidget {
  final int index;
  Friend friend;
   StatusCard({Key key, this.index = -1, this.friend}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = SizeConfig.screenHeight;
    double width = SizeConfig.screenWidth;
    bool isFirstCard = index == 0 ? true : false;
    //https://scontent.fnbo4-1.fna.fbcdn.net/v/t1.0-9/99236813_762461721225991_1010145427204341760_n.jpg?_nc_cat=103&_nc_sid=110474&_nc_eui2=AeFjAM8r6_nPwbQZf-GA6MU4a9KqGZyS5q1r0qoZnJLmrcRy3rA712JHz8WKGTL7lRx3c8aIFdX3EYQyHqRHT8zX&_nc_oc=AQlYYTX7g7rgRbV3krdkgGBFTDGsnl8RiHk61j5TyKrsuDhwjQnzTI5XzDZu6CuKIgs&_nc_ht=scontent.fnbo4-1.fna&oh=542a34ce442da0093b27cb4e6b1253c1&oe=5F1EC7D6
    return Stack(
      children: <Widget>[
     !isFirstCard?   Container(
          margin: EdgeInsets.all(10),
          decoration: BoxDecoration(
              color: Colors.white,
              image: DecorationImage(
                fit: BoxFit.cover,
                image: NetworkImage(friend.status[0].statusSource)
              ),
              borderRadius: BorderRadius.circular(20)),
          width: width * 0.3,
          height: height * 0.5,
        ):  Container(
          margin: EdgeInsets.all(10),
          decoration: BoxDecoration(
              color: Colors.white,
              image: DecorationImage(
                fit: BoxFit.cover,
                image: NetworkImage(
                    "https://scontent.fnbo4-1.fna.fbcdn.net/v/t1.0-9/92516457_1516712285162897_7055747972943839232_n.jpg?_nc_cat=104&_nc_sid=85a577&_nc_eui2=AeHNBup4AlvkP3o5BNm14h_ZinKdDvbjnn6Kcp0O9uOefowujMifES-ipE6m9vudqdz8f5RJ6mQ22aVhWbLE5kV-&_nc_oc=AQmbL1GTVkQjcpdpR1Nvj6qhEIow2s9nTvO5xk1JCA_LxfLD2Ki0JBoFSCYji8pVMyo&_nc_ht=scontent.fnbo4-1.fna&oh=8f83cecf89c4392bd405a964df8b7bbe&oe=5F1E9862"),
              ),
              borderRadius: BorderRadius.circular(20)),
          width: width * 0.3,
          height: height * 0.5,
        ),
       !isFirstCard? Container(
          alignment: Alignment.bottomLeft,
          margin: EdgeInsets.all(10),
          width: width * 0.3,
          height: height * 0.25,
         
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            gradient: LinearGradient(
              stops: [0.3, 0.4, 0.5, 0.7],
              begin: Alignment.center,
              end: Alignment.bottomCenter,
              colors: [
                Color.fromRGBO(151, 145, 145, 0.1),
                Color.fromRGBO(151, 145, 145, 0.3),
                Color.fromRGBO(151, 145, 145, 0.5),
                Color.fromRGBO(151, 145, 145, 0.7)
              ],
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(

              friend.friendName,
              style: TextStyle(fontSize: width*0.05, color: Colors.white,),
            ),
          ),
        ):Container(
         alignment: Alignment.bottomLeft,
         margin: EdgeInsets.all(10),
         width: width * 0.3,
         height: height * 0.25,

         decoration: BoxDecoration(
           borderRadius: BorderRadius.circular(20),
           gradient: LinearGradient(
             stops: [0.3, 0.4, 0.5, 0.7],
             begin: Alignment.center,
             end: Alignment.bottomCenter,
             colors: [
               Color.fromRGBO(151, 145, 145, 0.1),
               Color.fromRGBO(151, 145, 145, 0.3),
               Color.fromRGBO(151, 145, 145, 0.5),
               Color.fromRGBO(151, 145, 145, 0.7)
             ],
           ),
         ),
         child: Padding(
           padding: const EdgeInsets.all(8.0),
           child: Text(

             "Add to Status",
             style: TextStyle(fontSize: width*0.05, color: Colors.white,),
           ),
         ),
       ),
        isFirstCard
            ? Positioned(
                top: 20,
                left: 20,
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20)),
                  height: width * 0.07,
                  width: width * 0.07,
                  child: Icon(
                    Icons.add,
                    size: width * 0.05,
                    color: Colors.blue,
                  ),
                ),
              )
            : Container(),
             !isFirstCard
            ? Positioned(
                top: 20,
                right: 20,
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.circular(7)),
                  height: width * 0.07,
                  width: width * 0.06,
                  child: Text("1",textAlign: TextAlign.center,style: TextStyle(color: Colors.white,fontSize: width*0.05),),
                ),
              )
            : Container(),
      ],
    );
  }
}

import 'package:facebook/contants/page.dart';
import 'package:facebook/pages/friendspage.dart';
import 'package:facebook/pages/homepage.dart';
import 'package:facebook/pages/notification_page.dart';
import 'package:facebook/sections/create_post.dart';
import 'package:facebook/sections/friend_status.dart';
import 'package:facebook/sections/header.dart';
import 'package:facebook/pages/message_section.dart';
import 'package:facebook/sections/pagenavigation.dart';
import 'package:facebook/sections/post_action.dart';
import 'package:facebook/pages/video_section.dart';
import 'package:facebook/sections/settings.dart';
import 'package:facebook/tools/SizeConfig.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  HomePage({Key key}) : super(key: key);

@override
  Widget build(BuildContext context) {
    return Container(
      child: MaterialApp(
          theme: ThemeData(
            primaryColor: Colors.white,
          ),
          home: FacebookHome()),
    );
  }
}
class FacebookHome extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Home();
  }
  
}


class Home extends StatefulWidget {
   Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  NavigationPage currentPage=NavigationPage.HOME;
 OnPageSelected onpageSelected({NavigationPage page}){
     setState(() {
       currentPage=page;
     });
   }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);

    Widget page=HomeWidget();
    switch (currentPage) {
      case NavigationPage.HOME :
        page=HomeWidget();
        break;
      case NavigationPage.FRIENDS:
        page=FriendsPage();
        break;
      case NavigationPage.MESSAGES:
        page=MessagePage();
        break;
      case NavigationPage.VIDEOS:
        page=VideoSection();
        break;
      case NavigationPage.NOTIFICATIONS:
        page= NotificationPage();
        break;

      case NavigationPage.SETTING:
        page=SettingSection();
        break;
      default:
      page=Container();
    }
   
    return Container(
        child: Scaffold(
      body: Container(

          margin: EdgeInsets.only(top: SizeConfig.paddinTop),
          child: Flex(
            direction: Axis.vertical,
            children: <Widget>[
              Flexible(
                  flex:4,child: Header()),
              Flexible(flex:3,child:PageNavigation(onPageSelected: onpageSelected,)),
              Flexible(child: Divider()),

              Expanded(flex :32,child:page ,)

            ],
          )),
    ));
  }
}

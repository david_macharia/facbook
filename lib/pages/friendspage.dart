import 'package:facebook/contants/dummysources.dart';
import 'package:facebook/models/friends.dart';
import 'package:facebook/tools/SizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class FriendsPage extends StatefulWidget {
  FriendsPage({Key key}) : super(key: key);

  @override
  _FriendsPageState createState() => _FriendsPageState();
}

class _FriendsPageState extends State<FriendsPage> {
  int _friendRequestCount;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _friendRequestCount = friends.length;

    SizeConfig.init(context);
    double height = SizeConfig.screenHeight;
    double width = SizeConfig.screenWidth;
    return Container(
        child: Flex(
          direction: Axis.vertical,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[

        Flexible(
          child: Container(

            child: ListView.separated(
                itemCount: friends.length+(hasFriendRequest()==-1?0:1),
                separatorBuilder: (bConetx, count) {
                   if (count+1 == hasFriendRequest()) {
                    return ImportContact();
                  } else {
                    return Container();
                  }
                },
                itemBuilder: (vct, count) {
                  int current=count;
                  Friend friend = friends[current==0?0:current-1];
                    
                  if (count == 0) {
                    return hasFriendRequest()>0?FriendRequest(
                        height: height,
                        width: width,
                        friendRequestCount:hasFriendRequest()):Container();
                  } else if (friend.friendRequestReceived==true) {
                    return  NewFriendRequest(friend:friend,width: width, height: height);
                  } else if (count == _friendRequestCount - 1) {
                    return Text("null");
                  } else {
                    return Container();
                  }
                }),
          ),
        )
      ],
    ));
  }
}

class NewFriendRequest extends StatelessWidget {
  final Friend friend;
  const NewFriendRequest({
    Key key,
    @required this.friend,
    @required this.width,
    @required this.height,
  }) : super(key: key);

  final double width;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(width * 0.02),
          child: CircleAvatar(
            minRadius: width * 0.145,
            backgroundImage: NetworkImage(
                friend.profileImage),
          ),
        ),
        Expanded(
          child: Container(
              padding: EdgeInsets.all(width * 0.02),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(friend.friendName),
                  Container(
                    height: height * 0.1,
                    width: width * 0.8,
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: width * 0.04),
                      child: Stack(
                        children: <Widget>[
                          Positioned(
                            left: 0,
                            child: CircleAvatar(
                              radius: width * 0.040,
                              backgroundImage: NetworkImage(
                                  "https://scontent.fnbo4-1.fna.fbcdn.net/v/t1.0-9/92516457_1516712285162897_7055747972943839232_n.jpg?_nc_cat=104&_nc_sid=85a577&_nc_eui2=AeHNBup4AlvkP3o5BNm14h_ZinKdDvbjnn6Kcp0O9uOefowujMifES-ipE6m9vudqdz8f5RJ6mQ22aVhWbLE5kV-&_nc_oc=AQmbL1GTVkQjcpdpR1Nvj6qhEIow2s9nTvO5xk1JCA_LxfLD2Ki0JBoFSCYji8pVMyo&_nc_ht=scontent.fnbo4-1.fna&oh=8f83cecf89c4392bd405a964df8b7bbe&oe=5F1E9862"),
                            ),
                          ),
                          Positioned(
                            left: width * 0.065,
                            child: CircleAvatar(
                              radius: width * 0.040,
                              backgroundImage: NetworkImage(
                                  "https://scontent.fnbo4-1.fna.fbcdn.net/v/t1.0-9/92516457_1516712285162897_7055747972943839232_n.jpg?_nc_cat=104&_nc_sid=85a577&_nc_eui2=AeHNBup4AlvkP3o5BNm14h_ZinKdDvbjnn6Kcp0O9uOefowujMifES-ipE6m9vudqdz8f5RJ6mQ22aVhWbLE5kV-&_nc_oc=AQmbL1GTVkQjcpdpR1Nvj6qhEIow2s9nTvO5xk1JCA_LxfLD2Ki0JBoFSCYji8pVMyo&_nc_ht=scontent.fnbo4-1.fna&oh=8f83cecf89c4392bd405a964df8b7bbe&oe=5F1E9862"),
                            ),
                          ),
                          CircleAvatar(
                            radius: width * 0.03,
                            backgroundImage: NetworkImage(
                                "https://scontent.fnbo4-1.fna.fbcdn.net/v/t1.0-9/92516457_1516712285162897_7055747972943839232_n.jpg?_nc_cat=104&_nc_sid=85a577&_nc_eui2=AeHNBup4AlvkP3o5BNm14h_ZinKdDvbjnn6Kcp0O9uOefowujMifES-ipE6m9vudqdz8f5RJ6mQ22aVhWbLE5kV-&_nc_oc=AQmbL1GTVkQjcpdpR1Nvj6qhEIow2s9nTvO5xk1JCA_LxfLD2Ki0JBoFSCYji8pVMyo&_nc_ht=scontent.fnbo4-1.fna&oh=8f83cecf89c4392bd405a964df8b7bbe&oe=5F1E9862"),
                          ),
                          Positioned(
                              top: height * 0.01,
                              left: width * 0.16,
                              child: Text("51 mutual friends",style: TextStyle(fontSize:width*0.04 ),)),
                        ],
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment:
                        MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Flexible(
                        flex: 15,
                        fit: FlexFit.tight,
                        child: FlatButton(
                          color: Colors.indigo,
                          shape: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Colors.transparent,
                                  width: 0),
                              borderRadius:
                                  BorderRadius.circular(10)),
                          child: Text(
                            "Confirm",
                            style: TextStyle(color: Colors.white,fontSize:width*0.04 ),
                          ),
                          onPressed: () {},
                        ),
                      ),
                      Spacer(
                        flex: 1,
                      ),
                      Flexible(
                        flex: 15,
                        fit: FlexFit.tight,
                        child: FlatButton(
                          color: Colors.grey[200],
                          shape: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Colors.transparent),
                              borderRadius:
                                  BorderRadius.circular(10)),
                          child: Text(
                            "Delete",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,fontSize:width*0.04 ),
                          ),
                          onPressed: () {},
                        ),
                      )
                    ],
                  )
                ],
              )),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 8.0),
          child: Text("3d"),
        )
      ],
    );
  }
}

class ImportContact extends StatelessWidget {
  const ImportContact({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    double height = SizeConfig.screenHeight;
    double width = SizeConfig.screenWidth;
    return Container(
      height: height * 0.27,
      color: Colors.grey,
      child: Container(
        padding: EdgeInsets.all(width * 0.03),
        height: height * 0.17,
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border(
                top: BorderSide(color: Colors.grey[300], width: height * 0.03),
                bottom:
                    BorderSide(color: Colors.grey[300], width: height * 0.03))),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "Import Contacts",
              style: TextStyle(
                  fontSize: width * 0.0, fontWeight: FontWeight.bold),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Flexible(
                  child: Text(
                    "Let Facebook automatically upload new and updated contacts as you add them to your phone",
                    style: TextStyle(
                        fontSize: width * 0.04, fontWeight: FontWeight.normal),
                  ),
                ),
                Flexible(
                  flex: 1,
                  fit: FlexFit.loose,
                  child: Padding(
                    padding:  EdgeInsets.all(width*0.01),
                    child: FlatButton(
                      color: Colors.indigo,
                      shape: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.transparent, width: 0),
                          borderRadius: BorderRadius.circular(10)),
                      child: Text(
                        "Get Started",
                        style: TextStyle(color: Colors.white,fontSize: width*0.05),
                      ),
                      onPressed: () {},
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class FriendRequest extends StatelessWidget {
  const FriendRequest({
    Key key,
    @required this.height,
    @required this.width,
    @required int friendRequestCount,
  })  : _friendRequestCount = friendRequestCount,
        super(key: key);

  final double height;
  final double width;
  final int _friendRequestCount;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height * 0.07,
      padding: EdgeInsets.all(width * 0.03),
      child: Row(
        children: <Widget>[
          Text(
            "Friend Requests ",
            style:
                TextStyle(fontWeight: FontWeight.bold, fontSize: width * 0.05),
          ),
          Text(
            "$_friendRequestCount",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.red,
                fontSize: width * 0.05),
          )
        ],
      ),
    );
  }
}

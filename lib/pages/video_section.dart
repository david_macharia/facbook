import 'package:facebook/models/owner.dart';
import 'package:facebook/models/reaction.dart';
import 'package:facebook/models/user.dart';
import 'package:facebook/models/video.dart';
import 'package:facebook/tools/SizeConfig.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/src/scheduler/ticker.dart';
import 'package:flutter/widgets.dart';
import 'package:video_player/video_player.dart';

class VideoSection extends StatelessWidget {
  var _tabcontroller;
  List<Video> videoList = [
    Video(
        videoUrl: "assets/videos/parser_video_encode.mp4",
        videoDescription: "Parser Implementation ",
        owner:  Owner(firstName: "Abdul", lastName: "Raheem",
            profileImage: "https://scontent.fnbo9-1.fna.fbcdn.net/v/t1.0-1/c0.0.160.160a/p160x160/92516457_1516712285162897_7055747972943839232_n.jpg?_nc_cat=104&_nc_sid=dbb9e7&_nc_eui2=AeHNBup4AlvkP3o5BNm14h_ZinKdDvbjnn6Kcp0O9uOefowujMifES-ipE6m9vudqdz8f5RJ6mQ22aVhWbLE5kV-&_nc_oc=AQmZX49NSJEPJ3CnOsWgxMwpgG88YUEiXJuc21o9BIu-ZbT0v-8JfW7i-nPi_BH88x4PrNNe-3ERV4q2h6ZMy6hu&_nc_pt=5&_nc_ht=scontent.fnbo9-1.fna&oh=64bcdf7addadf6e39b4903d8c81fff21&oe=5F3133A4",
           ),
        reactions: [
          Reaction(reactionType: ReactionType.ANGRY),
          Reaction(reactionType: ReactionType.CARE)
        ]),
    Video(
        videoUrl: "assets/videos/parser_video_encode.mp4",
        videoDescription: "Parser Implementation ",
        owner:  Owner(firstName: "Abdul", lastName: "Raheem",
          profileImage: "https://scontent.fnbo9-1.fna.fbcdn.net/v/t1.0-1/c0.0.160.160a/p160x160/92516457_1516712285162897_7055747972943839232_n.jpg?_nc_cat=104&_nc_sid=dbb9e7&_nc_eui2=AeHNBup4AlvkP3o5BNm14h_ZinKdDvbjnn6Kcp0O9uOefowujMifES-ipE6m9vudqdz8f5RJ6mQ22aVhWbLE5kV-&_nc_oc=AQmZX49NSJEPJ3CnOsWgxMwpgG88YUEiXJuc21o9BIu-ZbT0v-8JfW7i-nPi_BH88x4PrNNe-3ERV4q2h6ZMy6hu&_nc_pt=5&_nc_ht=scontent.fnbo9-1.fna&oh=64bcdf7addadf6e39b4903d8c81fff21&oe=5F3133A4",
        ),
        reactions: [
          Reaction(reactionType: ReactionType.ANGRY),
          Reaction(reactionType: ReactionType.CARE)
        ]),
    Video(
        videoUrl: "assets/videos/parser_video_encode.mp4",
        videoDescription: "Parser Implementation ",
        owner:  Owner(firstName: "Abdul", lastName: "Raheem",
          profileImage: "https://scontent.fnbo9-1.fna.fbcdn.net/v/t1.0-1/c0.0.160.160a/p160x160/92516457_1516712285162897_7055747972943839232_n.jpg?_nc_cat=104&_nc_sid=dbb9e7&_nc_eui2=AeHNBup4AlvkP3o5BNm14h_ZinKdDvbjnn6Kcp0O9uOefowujMifES-ipE6m9vudqdz8f5RJ6mQ22aVhWbLE5kV-&_nc_oc=AQmZX49NSJEPJ3CnOsWgxMwpgG88YUEiXJuc21o9BIu-ZbT0v-8JfW7i-nPi_BH88x4PrNNe-3ERV4q2h6ZMy6hu&_nc_pt=5&_nc_ht=scontent.fnbo9-1.fna&oh=64bcdf7addadf6e39b4903d8c81fff21&oe=5F3133A4",
        ),
        reactions: [
          Reaction(reactionType: ReactionType.ANGRY),
          Reaction(reactionType: ReactionType.CARE)
        ]),
  ];
  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    double height = SizeConfig.screenHeight;
    double width = SizeConfig.screenWidth;
    return Flex(
      direction: Axis.vertical,
      children: <Widget>[
        Expanded(
          flex: 4,
          child: Container(
            color: Colors.white,
            child: Padding(
              padding: EdgeInsets.symmetric(
                  vertical: height * 0.01, horizontal: width * 0.04),
              child: Row(
                children: <Widget>[
                  Text(
                    "Video Notifications",
                    style: TextStyle(
                        fontSize: width * 0.05, fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: Divider(
            thickness: 1,
            height: 0,
          ),
        ),
        Expanded(
          flex: 40,
          child: Container(
            color: Colors.white,
            alignment: Alignment.topCenter,
            child: DefaultTabController(
              length: 2,
              child: Scaffold(
                appBar: AppBar(
                  toolbarOpacity: 0.0,
                  elevation: 0.0,
                  flexibleSpace: Container(
                    height: height * 0.09,
                    child: TabBar(
                      unselectedLabelColor: Colors.black,
                      labelColor: Colors.blue,
                      labelPadding: EdgeInsets.all(0),
                      tabs: <Widget>[
                        Container(
                            height: height * 0.07,
                            alignment: Alignment.center,
                            child: Text(
                              "Top Videos",
                              style: TextStyle(fontSize: width * 0.03),
                            )),
                        Container(
                            height: height * 0.07,
                            alignment: Alignment.center,
                            child: Text("Live Videos")),
                      ],
                    ),
                  ),
                ),
                body: Container(
                  child: TabBarView(
                    children: <Widget>[
                      VideoViewerWidget(videos: videoList),
                      Icon(Icons.directions_transit),
                    ],
                  ),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}

class VideoViewerWidget extends StatefulWidget {
  final List<Video> videos;
  const VideoViewerWidget({
    Key key,
    this.videos,
  }) : super(key: key);

  @override
  _VideoViewerWidgetState createState() => _VideoViewerWidgetState();
}

class _VideoViewerWidgetState extends State<VideoViewerWidget> {

 Video currentPlaying;
  @override
  void initState() {
    // TODO: implement initState

//        VideoPlayerController.asset("assets/videos/parser_video_encode.mp4")
//
//          ..initialize().then((_) {
//            // Once the video has been loaded we play the video and set looping to true.
////            _controller.play();
////            _controller.setLooping(true);
//            // Ensure the first frame is shown after the video is initialized.
//            setState(() {});
//          });
//

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    double height = SizeConfig.screenHeight;
    double width = SizeConfig.screenWidth;
    return Container(
      height: 150,
      color: Colors.white,
      child: ListView.builder(
        itemBuilder: (ctx, count) {
          Video video=widget.videos[count];
          video.controller.initialize();

          return MyVideo(video: video,);
        },
        itemCount: widget.videos.length,
      ),
    );
  }
}
class MyVideo extends StatefulWidget {
  Video video;
  MyVideo({@required this.video});
  @override
  _MyVideoState createState() => _MyVideoState();
}

class _MyVideoState extends State<MyVideo> {
  Video video;
  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    double height = SizeConfig.screenHeight;
    double width = SizeConfig.screenWidth;
    video=widget.video;
    return Container(
      width: width,

      height: height * 0.5,
      child: Flex(
        direction: Axis.vertical,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal: width * 0.03),
            child: Flex(
              direction: Axis.vertical,
              children: <Widget>[
                Flexible(
                  flex: 0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      CircleAvatar(
                        radius: width * 0.08,
                        backgroundColor: Colors.grey,
                        backgroundImage: video.owner.profileImage!=null?NetworkImage(video.owner.profileImage):null,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: width * 0.03,
                            vertical: height * 0.01),
                        child: Container(
                          height: height * 0.06,
                          alignment: Alignment.centerLeft,
                          child: Flex(
                            direction: Axis.vertical,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Flexible(
                                  flex: 0, child: Text("Abdul Raheem")),
                              Spacer(),
                              Flexible(
                                flex: 0,
                                child: Row(
                                  children: <Widget>[
                                    Text("7 hours ago"),
                                    Icon(Icons.map)
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Spacer(),
                      Icon(Icons.more_horiz)
                    ],
                  ),
                )
              ],
            ),
          ),
          Container(child: Padding(
            padding:  EdgeInsets.symmetric(horizontal:width*0.05 ),
            child: Text(video.videoDescription,style: TextStyle(fontSize: width*0.05),),
          ),),
          Flexible(
              flex: 1,
              child: Container(
                color: Colors.grey,
                child: Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    VideoPlayer(widget.video.controller),
                    GestureDetector(

                      onTap: () async{
                        if(!video.isPlaying()){

                          video.controller.play();

                          video.play();



                        }else{
                          video.controller.pause().then((onValue){
                            video.stop();
                          });


                        }
                        setState(() {




                        });
                      },
                      child: Container(
                        height: width*0.20,
                        width: width*0.20,
                        child: Icon(video.isPlaying()?Icons.play_arrow:Icons.pause,color: Colors.white,size: width*0.08,),
                        decoration: BoxDecoration(
                            color: Colors.black,
                            border: Border.all(color: Colors.white,width: 2),
                            borderRadius: BorderRadius.circular(width*0.20)),
                      ),
                    )
                  ],
                ),
              )),
          Container(
            child: Flex(
              direction: Axis.horizontal,
              children: <Widget>[
                Flexible(
                    flex: 50,
                    child: Container(
                      margin:  EdgeInsets.all(width*0.02),
                      height: height*0.07,
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(50),color: Colors.grey[200]),

                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[Icon(Icons.thumb_up,color: Colors.blue,),
                          Padding(
                            padding:  EdgeInsets.symmetric(horizontal:width*0.03),
                            child: Text("1",style: TextStyle(fontSize: width*0.05),),
                          )
                        ],
                      ),
                    )),
                Flexible(
                    flex: 50,
                    child: Container(
                      margin:  EdgeInsets.all(width*0.02),
                      height: height*0.07,
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(50),color: Colors.grey[200]),

                      child:Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[Icon(Icons.comment)],
                      ) ,)),
                Flexible(
                    flex: 50,
                    child: Container(
                      margin:  EdgeInsets.all(width*0.02),
                      height: height*0.07,
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(50),color: Colors.grey[200]),

                      child:Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[Icon(CupertinoIcons.share_up)],
                      ) ,))
              ],
            ),
          )
        ],
      ),
    );
  }
}


class Ticker extends TickerProvider {
  @override
  createTicker(onTick) {
    // TODO: implement createTicker
    return null;
  }
}

class MyTabController extends TabController {
  MyTabController() : super(length: 2, vsync: Ticker());
}

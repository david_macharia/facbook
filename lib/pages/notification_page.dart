import 'package:facebook/contants/dummysources.dart';
import 'package:facebook/models/notification.dart' as FacebookNotification;
import 'package:facebook/tools/SizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class NotificationPage extends StatelessWidget {
  List<FacebookNotification.Notification> notifications;
  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    double height = SizeConfig.screenHeight;
    double width = SizeConfig.screenWidth;
    notifications = getNotification();

    var newnotification = getNewNotifications();
    var itemCount =
        notifications.length + 2 + (newnotification.length > 0 ? 1 : 0);
    var oldCountStart = itemCount - newnotification.length + 2;
    return Container(
      color: Colors.grey[300],
      child: ListView.separated(
          itemCount: itemCount,
          padding: EdgeInsets.all(0),
          separatorBuilder: (ctx, count) {
            if (count == newNotifactionEndsAt() - 1) {
              return Container(
                alignment: Alignment.centerLeft,
                height: height * 0.07,
                color: Colors.white,
                margin: EdgeInsets.symmetric(
                    vertical: height * 0, horizontal: width * 0),
                padding: EdgeInsets.symmetric(horizontal: width * 0.03),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Earlier",
                      style: TextStyle(
                          fontSize: width * 0.05,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                    Divider(
                      color: Colors.grey[400],
                    )
                  ],
                ),
              );
            }
            return SizedBox();
          },
          itemBuilder: (ctx, count) {
            if (count == 0) {
              return Container(
                alignment: Alignment.center,
                height: height * 0.07,
                color: Colors.white,
                margin: EdgeInsets.symmetric(
                    vertical: height * 0.015, horizontal: width * 0.03),
                child: Text(
                  "Mark All as Read",
                  style: TextStyle(
                      fontSize: width * 0.05,
                      color: Colors.blue,
                      fontWeight: FontWeight.bold),
                ),
              );
            } else if (newnotification.length > 0 && count == 1) {
              return Container(
                alignment: Alignment.centerLeft,
                height: height * 0.07,
                color: Colors.white,
                margin: EdgeInsets.symmetric(
                    vertical: height * 0, horizontal: width * 0),
                padding: EdgeInsets.symmetric(horizontal: width * 0.03),
                child: Text(
                  "New",
                  style: TextStyle(
                      fontSize: width * 0.05,
                      color: Colors.black,
                      fontWeight: FontWeight.bold),
                ),
              );
            } else {
              if (hasNewNotifiaction()) {
                if (count + 1 >= newNotificationStartAt() &&
                    count + 1 <= newNotifactionEndsAt()) {
                  FacebookNotification.Notification facebookNotification =
                      notifications[count];
                  return NotificationWidget(
                    facebookNotification: facebookNotification,
                    isLast: newnotification.length - 1 == count,
                  );
                } else {
                  FacebookNotification.Notification facebookNotification =
                      notifications[count - 3];
                  return NotificationWidget(
                    facebookNotification: facebookNotification,
                    isLast: newnotification.length - 1 == count,
                  );
                }
              } else {
                FacebookNotification.Notification facebookNotification =
                    notifications[count];
                return NotificationWidget(
                  facebookNotification: facebookNotification,
                  isLast: newnotification.length - 1 == count,
                );
              }
              ;
            }
            return SizedBox();
          }),
    );
  }

  bool hasNewNotifiaction() {
    if (getNewNotifications().length > 0) {
      return true;
    }
    return false;
  }

  int newNotificationStartAt() {
    var newnotification = getNewNotifications();
    return 2 + (newnotification.length > 0 ? 1 : 0);
  }

  int newNotifactionEndsAt() {
    var newnotification = getNewNotifications();
    return 2 +
        (newnotification.length > 0 ? 1 : 0) +
        newnotification.length -
        1;
  }
}

class NotificationWidget extends StatelessWidget {
  FacebookNotification.Notification facebookNotification;
  final bool isLast;
  NotificationWidget({Key key, this.isLast = false, this.facebookNotification})
      : super(key: key);

  double height;
  double width;

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    double height = SizeConfig.screenHeight;
    double width = SizeConfig.screenWidth;
    return Container(
      height: height * 0.25,
      decoration: BoxDecoration(
          color: !facebookNotification.isRead
              ? Colors.cyan[50]
              : Colors.white),
      margin: EdgeInsets.symmetric(vertical: height * 0, horizontal: width * 0),
      child: Column(
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(width * 0.02),
                child: CircleAvatar(
                  radius: width * 0.10,
                  backgroundColor: Colors.grey,
                  backgroundImage: facebookNotification.notifactionLargeIcon!=null?NetworkImage(facebookNotification.notifactionLargeIcon):null,
                ),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[

                    Padding(
                      padding: EdgeInsets.all(width * 0.02),
                      child: RichText(
//                        facebookNotification.notificationSource,
                        maxLines: 3,
                       overflow: TextOverflow.ellipsis,

//                  style: TextStyle(fontSize: width * 0.05),
                        text: TextSpan(
                            style: TextStyle(

                                fontSize: width * 0.05, color: Colors.black),
                            children: <InlineSpan>[
                              TextSpan(
                                text: "CGTN",
                                style: TextStyle(
                                    decoration: TextDecoration.lineThrough,
                                    fontSize: width * 0.05,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black),
                              ),
                              TextSpan(
                                text: facebookNotification.notificationSource,
                                style: TextStyle(
                                    fontSize: width * 0.05,

                                    color: Colors.black),
                              )
                            ],
                            ),
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(width * 0.02),
                          child: facebookNotification.notificationIcon,
                        ),
                        Text(facebookNotification.notifacationTime)
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.all(width * 0.02),
                child: Icon(Icons.more_vert),
              )
            ],
          ),
          !isLast
              ? Divider(
                  color: Colors.grey[400],
                )
              : SizedBox()
        ],
      ),
    );
  }
}

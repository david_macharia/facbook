import 'package:facebook/contants/dummysources.dart';
import 'package:facebook/models/friends.dart';
import 'package:facebook/models/message_content.dart';
import 'package:facebook/models/text_message.dart';
import 'package:facebook/tools/SizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class MessagePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double height = SizeConfig.screenHeight;
    double width = SizeConfig.screenWidth;
    return Flex(

      direction: Axis.vertical,
      children: <Widget>[
        Expanded(
       flex: 20,
          child: Container(
            color: Colors.white,

            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: friends.length + 1,
                padding: EdgeInsets.symmetric(
                    vertical: height * 0.01, horizontal: width*0.03),
                itemBuilder: (ctx, count) {
                  Widget current;
                  Friend friend = friends[count == 0 ? count + 1 : count - 1];
                  if (count == 0) {
                    current = MyStatus();
                  } else {
                    current = FriendStatus(
                      friend: friend,
                    );
                  }
                  return Column(
                    children: <Widget>[
                      current,
                      Container(
                        width: width * 0.15,

                        child: Text(
                          count == 0 ? "Add to Status" : friend.friendName,
                          style: TextStyle(fontSize: width * 0.03),
                          softWrap: count == 0 ? true : false,
                          overflow: count == 0
                              ? TextOverflow.visible
                              : TextOverflow.ellipsis,
                        ),
                      ),
                    ],
                  );
                }),
          ),
        ),
        Expanded(
   flex: 1,
          child: Divider(
            thickness: height * 0.009,
          ),
        ),
        Expanded(
  flex: 63,
          child: Container(

            child: ListView.builder(
                padding: EdgeInsets.all(0),
                itemCount: getFriendWithMessages().length,
                itemBuilder: (ctx, count) {
                  Friend friend = getFriendWithMessages()[count];
                  MessageContent messageContent =
                      friend.getFirstMessage().content;
                  String displayMessage;
                  switch (messageContent.messageType) {
                    case MessageType.PLAIN_TEXT:
                      TextMessage testMessage = messageContent as TextMessage;
                      displayMessage = testMessage.message;
                      break;
                    case MessageType.VIDEO:
                      // TODO: Handle this case.
                      break;
                    case MessageType.PICTURE:
                      // TODO: Handle this case.
                      break;
                    case MessageType.HYPER_MESSAGE:
                      // TODO: Handle this case.
                      break;
                  }
                  return ListTile(

                    contentPadding: EdgeInsets.symmetric(horizontal:width * 0.03,vertical:height*0.02 ),
                    leading: FriendsImage(
                      friend: friend,
                    ),
                    subtitle: Text(displayMessage,style: TextStyle(fontSize:width*0.05),),
                    title: Text(
                      friend.friendName,
                      style: TextStyle(fontWeight: FontWeight.bold,fontSize:width*0.05),
                    ),
                  );
                }),
          ),
        )
      ],
    );
  }
}

class FriendsImage extends StatelessWidget {
  final Friend friend;
  const FriendsImage({
    @required this.friend,
  });

  @override
  Widget build(BuildContext context) {
    double height = SizeConfig.screenHeight;
    double width = SizeConfig.screenWidth;
    return Stack(
      overflow: Overflow.visible,
      children: <Widget>[
        Container(
          height: height * 0.08,
          width: height * 0.08,
          decoration: BoxDecoration(
              image:
                  DecorationImage(image: AssetImage("assets/images/face.jpg"),fit: BoxFit.cover),
              borderRadius: BorderRadius.circular(100),
              color: Colors.green),
        ),
        friend.isActive
            ? Positioned(
                top: height * 0.034,
                left: height * 0.040,
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal:width * 0.05,vertical: height*0.01),
                  width: width * 0.032,
                  height: width * 0.032,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      color: Colors.green),
                ),
              )
            : friend.lastSeen != null
                ? Positioned(
                    top: height * 0.030,
                    left: width * 0.05,
                    child: Container(
                        alignment: Alignment.center,
                        margin: EdgeInsets.symmetric(horizontal:width * 0.05,vertical: height*0.01),
                        height: height * 0.03,
                        width: width * 0.07,
                        child: Text(
                          "${friend.lastSeen}",
                          style: TextStyle(fontSize: width * 0.03),
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            color: Colors.green)))
                : SizedBox()
      ],
    );
  }
}

class MyStatus extends StatelessWidget {
  const MyStatus({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = SizeConfig.screenHeight;
    double width = SizeConfig.screenWidth;
    return Container(
      alignment: Alignment.center,
      width: height * 0.11,
      height: height * 0.11,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100), color: Colors.grey),
      child: Stack(
        overflow: Overflow.visible,
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(horizontal:width * 0.000),
            width: height * 0.22,
            height: height * 0.22,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100), color: Colors.grey),
            child: Icon(
              Icons.camera_enhance,
              color: Colors.white,
              size: width * 0.06,
            ),
          ),
          Positioned(
            top: height * 0.075,
            left: width * 0.10,
            child: Container(
              margin: EdgeInsets.symmetric(horizontal:width * 0.03),
              width: width * 0.05,
              height: height * 0.03,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100), color: Colors.blue),
              child: Icon(
                Icons.add,
                size: width * 0.03,
                color: Colors.white,
              ),
            ),
          )
        ],
      ),
    );
  }
}

class FriendStatus extends StatelessWidget {
  Friend friend;
  FriendStatus({@required this.friend});

  @override
  Widget build(BuildContext context) {
    double height = SizeConfig.screenHeight;
    double width = SizeConfig.screenWidth;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: width * 0.03),
      child: Container(
        width: width * 0.17,
        height: width * 0.17,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(100),
            color: friend.isStatusViewed() ? Colors.grey : Colors.blue),
        child: Stack(
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: <Widget>[
            Container(
              width: width * 0.16,
              height: width * 0.16,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100), color: Colors.blue),
              child: Icon(
                Icons.camera_enhance,
                color: Colors.white,
                size: width * 0.06,
              ),
            ),
            Container(
              margin: EdgeInsets.all(width * 0.003),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  color: Colors.white),
              child: Container(
                margin: EdgeInsets.all(width * 0.006),
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(
                          friend.status[0].statusSource,
                        ),
                        fit: BoxFit.cover),
                    borderRadius: BorderRadius.circular(100),
                    color: Colors.white),
              ),
            ),
            friend.isActive
                ? Positioned(
                    top: height * 0.026,
                    left: width * 0.095,
                    child: Container(
                      margin: EdgeInsets.all(width * 0.05),
                      width: width * 0.032,
                      height: width * 0.032,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(100),
                          color: Colors.green),
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }
}

import 'package:facebook/sections/create_post.dart';
import 'package:facebook/sections/friend_status.dart';
import 'package:facebook/sections/post_action.dart';
import 'package:facebook/tools/SizeConfig.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class HomeWidget extends StatelessWidget {
  const HomeWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    double height = SizeConfig.screenHeight;
    double width = SizeConfig.screenWidth;
    return Container(
      color: Colors.white,
      height: height * 0.784,
      child: ListView.separated(
        separatorBuilder: (context, counter) {
          return Divider();
        },
        itemCount: 10,
        itemBuilder: (context, count) {
          switch (count) {
            case 0:
              return CreatePost();
            case 1:
              return PostAction();
            case 2:
              return Container(
                height: height * 0.02,
                color: Colors.black38,
              );
            case 3:
              return FriendStatus();
            case 4:
              return Container(
                  width: width,
                  height: height * 0.1,
                  color: Colors.black38,
                  alignment: Alignment.center,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      width: width * 0.4,
                      height: height * 1,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadiusDirectional.circular(50)),
                      child: Text(
                        "New Posts",
                        style: TextStyle(
                            fontSize: width * 0.03, color: Colors.white),
                      ),
                    ),
                  ));
            default:
              return StoryLine();
          }
        },
      ),
    );
  }
}

class StoryLine extends StatelessWidget {
  const StoryLine({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    double height = SizeConfig.screenHeight;
    double width = SizeConfig.screenWidth;
    return Container(
      padding: EdgeInsets.symmetric(horizontal: width * 0.03),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              CircleAvatar(
                radius: width * 0.05,
                backgroundImage: NetworkImage(
                    "https://scontent.fnbo4-1.fna.fbcdn.net/v/t1.0-9/99236813_762461721225991_1010145427204341760_n.jpg?_nc_cat=103&_nc_sid=110474&_nc_eui2=AeFjAM8r6_nPwbQZf-GA6MU4a9KqGZyS5q1r0qoZnJLmrcRy3rA712JHz8WKGTL7lRx3c8aIFdX3EYQyHqRHT8zX&_nc_oc=AQlYYTX7g7rgRbV3krdkgGBFTDGsnl8RiHk61j5TyKrsuDhwjQnzTI5XzDZu6CuKIgs&_nc_ht=scontent.fnbo4-1.fna&oh=542a34ce442da0093b27cb4e6b1253c1&oe=5F1EC7D6"),
              ),
              Container(
                height: height * 0.11,
                width: width * 0.84,
                padding: EdgeInsets.symmetric(
                    vertical: height * 0.01, horizontal: width * 0.02),
                alignment: Alignment.topLeft,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Antony Nyaga",
                          style: TextStyle(
                              fontSize: width * 0.04,
                              fontWeight: FontWeight.bold),
                        ),
                        Icon(
                          Icons.arrow_right,
                          size: width * 0.06,
                        ),
                        Flexible(
                          child: Text(
                            "Growing up in 1980 & 90s was best ❤",
                            overflow: TextOverflow.clip,
                            maxLines: 3,
                            softWrap: true,
                            style: TextStyle(
                                fontSize: width * 0.04,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Icon(
                          Icons.more_horiz,
                          size: width * 0.04,
                        ),
                      ],
                    ),
                    Text(
                      "14 hours Ago",
                      style: TextStyle(
                        fontSize: width * 0.03,
                      ),
                    )
                  ],
                ),
              ),
              Column(
                children: <Widget>[],
              )
            ],
          ),
          Flexible(
            child: Container(
              constraints: BoxConstraints.expand(height: height * 0.08),
              child: Text(
                "Tell us the experience na hutu tumududu,njururi 😂 😂",
                overflow: TextOverflow.clip,
                maxLines: 3,
                softWrap: true,
                style: TextStyle(
                    fontSize: width * 0.05, fontWeight: FontWeight.normal),
              ),
            ),
          ),
          Text("View translation", style: TextStyle(fontSize: width * 0.05)),
          Container(
            constraints: BoxConstraints.expand(height: height * 0.4),
            decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(
                        "https://scontent.fnbo4-1.fna.fbcdn.net/v/t1.0-0/s640x640/106075112_2308298602810861_8290025605438584729_n.jpg?_nc_cat=108&_nc_sid=ca434c&_nc_eui2=AeFA3yAkugo7xLuc9ZFAuWu9Bxm9Auf0FzUHGb0C5_QXNUbEmhTAaX_KGc-E7fCo64JOgreS1Ts2T_pg3L3IuQWt&_nc_oc=AQnVBErPtZNh29Nl-xLfl3k3-KgewipmYmE9eX784k7FXxWTqDv7PMJVEvYD8MavcwQ&_nc_ht=scontent.fnbo4-1.fna&_nc_tp=7&oh=b2c50af2d53dbf048db1881bb92f66bf&oe=5F1FF0E4"))),
          ),
          Container(
         child: Flex(
              direction: Axis.horizontal,
              children: <Widget>[
                Flexible(
                    flex: 50,
                    child: Container(
                      margin:  EdgeInsets.all(width*0.02),
                      height: height*0.07,
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(50),color: Colors.grey[200]),

                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[Icon(Icons.thumb_up,color: Colors.blue,),
                        Padding(
                          padding:  EdgeInsets.symmetric(horizontal:width*0.03),
                          child: Text("1",style: TextStyle(fontSize: width*0.05),),
                        )
                        ],
                      ),
                    )),
                Flexible(
                    flex: 50,
                    child: Container(
                      margin:  EdgeInsets.all(width*0.02),
                      height: height*0.07,
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(50),color: Colors.grey[200]),

                      child:Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Icon(Icons.comment)],
                    ) ,)),
                Flexible(
                    flex: 50,
                    child: Container(
                      margin:  EdgeInsets.all(width*0.02),
                      height: height*0.07,
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(50),color: Colors.grey[200]),

                      child:Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Icon(CupertinoIcons.share_up)],
                    ) ,))
              ],
            ),
          )
        ],
      ),
    );
  }
}
